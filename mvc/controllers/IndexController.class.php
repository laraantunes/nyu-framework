<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe padrão para actions.
 * Trata da Home do site através do método indexAction
 * Esta classe deve ser alterada para modificar o comportamento padrão da Home
 * 
 * Modificar o autor e o pacote conforme necessário
 * 
 * @author Maycow Alexandre Antunes <maycow@maycow.com.br>
 * @package NyuMVC
 * @version 1.0
 */
class IndexController extends NyuController{
    /**
     * Ação padrão da Home do site
     */
    public function indexAction() {
        NyuCore::welcomeNyu();
    }
}