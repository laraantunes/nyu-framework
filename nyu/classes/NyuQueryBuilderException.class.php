<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe para tratar excessões na geração de Querys
 * @package NyuCore
 * @author Maycow Alexandre Antunes <maycow@maycow.com.br>
 * @since 4.0
 */
class NyuQueryBuilderException extends NyuException{}