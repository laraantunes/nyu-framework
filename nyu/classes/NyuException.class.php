<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe para tratar excessões personalizadas
 * @package NyuCore
 * @author Maycow Alexandre Antunes <maycow@maycow.com.br>
 */
class NyuException extends Exception {}
