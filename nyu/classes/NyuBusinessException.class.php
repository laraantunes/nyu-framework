<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe para tratar excessões nas regras de negócio de objetos
 * @package NyuCore
 * @author Maycow Alexandre Antunes <maycow@maycow.com.br>
 * @since 6.0
 */
class NyuBusinessException extends NyuException{}