<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe para tratar excessões na validação de objetos
 * @package NyuCore
 * @author Maycow Alexandre Antunes <maycow@maycow.com.br>
 * @since 4.0
 */
class NyuValidateException extends NyuException{}