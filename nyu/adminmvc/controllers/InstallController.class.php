<?php

class InstallController extends \AdminController{
    
    /**
     * Primeira tela de instalação
     */
    public function indexAction() {
        // Se o banco existe, mas não existem usuários, redireciona
        if(\nyuadmin\NyuAdminBase::databaseExists() && !\nyuadmin\NyuAdminBase::nyuAdminInstalled()){
            $this->redirect(NYU_ADMIN_URL.'install/user/');
        // Se o banco ja existe e existem usuarios
        }elseif(\nyuadmin\NyuAdminBase::databaseExists() && \nyuadmin\NyuAdminBase::nyuAdminInstalled()){
            $this->redirect(NYU_ADMIN_URL.'user/login');
        }
        $this->loadTemplate("install",null,"Instalação",
            array(SITE_URL."nyu/adminmedia/js/icheck.min.js",
                  SITE_URL."nyu/adminmedia/js/modules/install.js"),
            array(SITE_URL."nyu/adminmedia/css/icheck/skins/square/blue.css",
                  SITE_URL."nyu/adminmedia/css/modules/install.css"),
            true);
    }
    
    /**
     * Processo de instalação
     */
    public function doAction(){
        \nyuadmin\NyuAdminBase::install();
        $this->redirect(NYU_ADMIN_URL.'install/user/');
    }
    
    /**
     * Tela do primeiro usuário cadastrado
     */
    public function userAction(){
        // Se não existe o banco, redireciona
        if(!\nyuadmin\NyuAdminBase::databaseExists()){
            $this->redirect(NYU_ADMIN_URL.'install/');
        }
        $this->loadTemplate("first_user",null,"Primeiro Usuário",array(SITE_URL.'nyu/adminmedia/js/modules/first-user.js'),null,true);
    }
    
    /**
     * Processo de instalação customizado
     */
    public function customAction(){
        $return = \nyuadmin\NyuAdminBase::install($this->post);
        $this->json($return);
    }
    
    /**
     * Criação do primeiro usuário
     */
    public function createUserAction(){
        $usr = new \nyuadmin\UserModel();
        $usr->setUsername($this->post['username']);
        $usr->setPassword($this->post['password']);
        $usr->setNickname($this->post['nickname']);
        $usr->setAdmin(true); // Seta o usuario inicial como administrador
        $usr->setEmail($this->post['email']);
        $return = array();
        if($usr->save()){
            \nyuadmin\NyuAdminBase::logUserIn($usr->getId());
            $return['status'] = true;
            $return['id'] = $usr->getUser();
            $return['message'] = 'Usuário criado com sucesso';
        }else{
            $return['status'] = false;
            $return['message'] = \NyuCore::getException();
        }
        $this->json($return);
    }
    
    /**
     * Reinstalação do Nyu
     */
    public function redoAction(){
        // Se não está logado, redireciona para a tela de login
        if(!nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        // Se não é administrador, ignora
        if(!\nyuadmin\NyuAdminBase::getLoggedUser()->isAdmin()){
            $this->redirect(NYU_ADMIN_URL);
        }
        $fm = new \NyuFileManager(SITE_FOLDER);
        
        if(!$fm->fileExists("nyu-install")){
            $fm->saveFile("nyu-install", "");
        }
        if($fm->fileExists("/nyu/admindata/nyuadmin.sqlite")){
            $fm->deleteFile("/nyu/admindata/nyuadmin.sqlite");
        }
        if($fm->fileExists("/nyu/admindata/customdatabase.nyudata")){
            $fm->deleteFile("/nyu/admindata/customdatabase.nyudata");
        }
        \nyuadmin\NyuAdminBase::logoffUser();
        $this->redirect(NYU_ADMIN_URL."install/");
    }
}