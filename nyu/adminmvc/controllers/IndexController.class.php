<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe padrão para actions.
 * Trata da Home do site através do método indexAction
 * Esta classe deve ser alterada para modificar o comportamento padrão da Home
 * 
 * Modificar o autor e o pacote conforme necessário
 * 
 * @author Maycow Alexandre Antunes <maycow@maycow.com.br>
 * @package NyuMVC
 * @version 1.0
 */
class IndexController extends AdminController{

    public function __construct() {
        parent::__construct();
            if($this->get[0] != 'about'){
            // Se não existe banco de dados da administração, redireciona para a instalação
            if(!\nyuadmin\NyuAdminBase::nyuAdminInstalled()){
                $this->redirect(NYU_ADMIN_URL."install/");
            }elseif(!nyuadmin\NyuAdminBase::isUserLogged()){
                $this->redirect(NYU_ADMIN_URL."user/login/");
            }
        }
    }
    /**
     * Ação padrão da Home do site
     */
    public function indexAction() {
        $user = \nyuadmin\NyuAdminBase::getLoggedUser();
        $this->loadTemplate("home", array("user" => $user));
    }
    
    /**
     * Redireciona para a action de login
     */
    public function loginAction(){
        $this->redirect(NYU_ADMIN_URL."user/login/");
    }

    protected function hohohoAction(){
        /*$lis = \nyuadmin\NyuAdminBase::__buildMenuHTML();
        foreach($lis as $li){
            echo htmlentities($li);
        }*/
        //echo \nyuadmin\NyuAdminBase::buildMenu();
        $obj = new \nyuadmin\MenuTreeItemModel();
        $obj->setId('ee46d9312077b2c9020b5c2ae9643bc49c97086d');
        $obj->load();
        echo "<pre>";
        var_dump($obj->buildBreadcrumbArray());
        echo "</pre>";
        $data['breadcrumb'] = \nyuadmin\NyuAdminBase::buildBreadCrumb($obj->buildBreadcrumbArray());
        echo $data['breadcrumb'];
    }
    
    public function aboutAction(){
        $this->loadTemplate("about",null,"Sobre o Nyu",
            null,
            null,
            true);
    }
}