<?php

class ConfigController extends AdminController{
    
    public function __construct() {
        parent::__construct();
        if(!nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        if(!\nyuadmin\NyuAdminBase::getLoggedUser()->isAdmin()){
            $this->redirect(NYU_ADMIN_URL);
        }
    }
    
	
    public function siteAction(){
        $data = array();
        $f = new NyuFileManager(SITE_FOLDER."/sitedata");
        $data['content'] = $f->loadFile('config.php');
        $this->loadTemplate("site_config", $data, 
                            "Configurações do Site", 
                            array(SITE_URL.'nyu/adminmedia/lib/src-min-noconflict/ace.js',
                                  SITE_URL.'nyu/adminmedia/js/modules/site-config.js'), 
                            array(SITE_URL.'nyu/adminmedia/css/codemirror.css'));
    }
    
    public function adminEditAction(){
        $data = array();
        $f = new NyuFileManager(SITE_FOLDER."/nyu/admindata");
        $data['content'] = $f->loadFile('customdatabase.nyudata');
        $this->loadTemplate("admin_config_edit", $data, 
                            "Dados de acesso ao Banco de Dados da Administração", 
                            array(SITE_URL.'nyu/adminmedia/lib/src-min-noconflict/ace.js',
                                  SITE_URL.'nyu/adminmedia/js/modules/admin-config-edit.js'), 
                            array(SITE_URL.'nyu/adminmedia/css/codemirror.css'));
    }

    public function adminAction(){
        $data = array();
        $fm = new NyuFileManager(SITE_FOLDER."/nyu/admindata");
        if($fm->fileExists("nyuadmin.sqlite")){
            $data['config_driver'] = 'sqlite';
        }
        if($fm->fileExists("customdatabase.nyudata")){
            $data['config_driver'] = 'mysql';
            $data['config_content'] = $fm->loadFile("customdatabase.nyudata");
        }
        // Constantes do sistema
        $data['SITE_FOLDER'] = SITE_FOLDER;
        $data['_LANG_'] = _LANG_;
        $data['SITE_PATH'] = SITE_PATH;
        $data['DS'] = DS;
        $data['NYU_ADMIN_SALT'] = NYU_ADMIN_SALT;
        $data['_IGNORE_NOT_FOUND_PAGE_'] = _IGNORE_NOT_FOUND_PAGE_;
        $data['_CACHE_VIEWS_'] = _CACHE_VIEWS_;
        $data['_401_ERROR_FILE_'] = _401_ERROR_FILE_;
        $data['_403_ERROR_FILE_'] = _403_ERROR_FILE_;
        $data['_404_ERROR_FILE_'] = _404_ERROR_FILE_;
        $data['_500_ERROR_FILE_'] = _500_ERROR_FILE_;
        $data['_DEFAULT_ERROR_FILE_'] = _DEFAULT_ERROR_FILE_;
        $data['_NYU_ADMIN_'] = _NYU_ADMIN_;
        $data['_LOG_'] = _LOG_;
        
        $this->loadTemplate("admin_config", $data, 
                            "Configurações da Administração do Nyu", 
                            array(SITE_URL.'nyu/adminmedia/js/modules/admin-config.js'));
    }

    /**
     * Salva as configurações do site
     */
    public function saveSiteAction(){
        $content = $this->post['content'];
        $f = new NyuFileManager(SITE_FOLDER."/sitedata");
        if($f->saveFile("config.php", $content)){
            $this->json(array("status" => true, "msg" => "Configurações salvas com sucesso!"));
        }else{
            $this->json(array("status" => false, "msg" => "Não foi possível salvar as configurações."));
        }
    }
    
    /**
     * Salva as configurações do site
     */
    public function saveAdminAction(){
        $content = $this->post['content'];
        $f = new NyuFileManager(SITE_FOLDER."/nyu/admindata");
        if($f->saveFile("customdatabase.nyudata", $content)){
            $this->json(array("status" => true, "msg" => "Configurações salvas com sucesso!"));
        }else{
            $this->json(array("status" => false, "msg" => "Não foi possível salvar as configurações."));
        }
    }
}

