<?php

/**
 * 2016 Nyu Framework
 */

/**
 * Classe que gerencia o carregamento de plugins na administração do Nyu
 */
class PluginController extends \AdminController {

    /**
     * Construtor da classe, valida se o usuário está logado
     */
    public function __construct() {
        parent::__construct();
        // Se não está logado, redireciona para o login
        if (!\nyuadmin\NyuAdminBase::isUserLogged()) {
            $this->redirect(NYU_ADMIN_URL . "user/login/");
        }
    }

    /**
     * Action base do carregamento de plugins da administração do Nyu
     */
    public function indexAction() {
        // Seta as variáveis para buscar a controller/action
        $plugin = $this->get[1];
        $controller = (isset($this->get[2]) ? ucwords($this->get[2]) : 'Index') . 'Controller';
        $action = (isset($this->get[3]) ? $this->get[3] : 'index') . 'Action';

        //Carrega o gerenciador de plugins
        $plm = new \nyuadmin\NyuAdminPluginManager($plugin);
        // Se o plugin existe, busca a controller
        if ($plm->pluginExists()) {
            // Carrega as controllers do plugin
            $plm->loadPluginControllersFolder();
            // Carrega as models do plugin
            $plm->loadPluginModelsFolder();
            $removeController = true;
            if (!class_exists($controller)) {
                $controller = 'IndexController';
                $removeController = false;
            }
            // Se o método não existe, chama o método index, com o parâmetro
            $removeAction = true;
            if (!method_exists(new $controller(), $action)) {
                $action = 'indexAction';
                $removeAction = false;
            }
            // Limpa os arrays $this->get / $GET
            unset($this->get[0]); // _nyuadmin
            unset($this->get[1]); // plugin
            if ($removeController) { // controller
                unset($this->get[2]);
            }
            if ($removeAction) { //action
                unset($this->get[3]);
            }
            $this->get = array_values($this->get);
            global $GET;
            $GET = $this->get; // Reescreve o array $GET
            // Carrega a controller
            $ctr = new $controller();
            // Carrega a action
            $ctr->$action();
        }
    }

    /**
     * Lista de plugins instalados
     */
    public function listAction() {
        $plugins = \nyuadmin\NyuAdminPluginManager::listPlugins();
        $list = array();
        if ($plugins) {
            foreach ($plugins as $plugin) {
                $pm = new \nyuadmin\NyuAdminPluginManager($plugin);
                $pluginArray = [];
                $pluginArray['id'] = $plugin;
                $pluginArray['name'] = $pm->getConfig('name') ? $pm->getConfig('name') : $plugin;
                $pluginArray['configAction'] = $pm->getConfig('configAction') ? $pm->getConfig('configAction') : null;
                $list[] = $pluginArray;
            }
        }
        $data['list'] = $list;
        $this->loadTemplate("plugin_list", $data, "Lista de Plugins", array(SITE_URL . 'nyu/adminmedia/js/modules/plugin-list.js'));
    }

    /**
     * Editar configurações de um plugin
     */
    public function editAction() {
        if(!$this->get[2]){
            $this->showMessage("Erro!", "Plugin não informado!", null, true);
        }
        $plugin = $this->get[2];
        $pm = new \nyuadmin\NyuAdminPluginManager($plugin);
        if(!$pm->pluginExists()){
            $this->showMessage("Erro!", "O Plugin informado não está instalado!", null, true);
        }
        $data = array();
        $data['content'] = $pm->getPluginConfig(false);
        $data['plugin'] = $plugin;
        $this->loadTemplate("plugin_edit", $data, "Editar configurações do Plugin", array(SITE_URL . 'nyu/adminmedia/lib/src-min-noconflict/ace.js',
            SITE_URL . 'nyu/adminmedia/js/modules/plugin-edit.js'), array(SITE_URL . 'nyu/adminmedia/css/codemirror.css'));
    }

    /**
     * Grava as configurações de um plugin
     */
    public function saveConfigAction(){
        $content = $this->post['content'];
        $plugin = $this->post['plugin'];
        $pm = new \nyuadmin\NyuAdminPluginManager($plugin);
        if($pm->savePluginConfigFile($content)){
            $this->json(array("status" => true, "msg" => "Configurações salvas com sucesso!"));
        }else{
            $this->json(array("status" => false, "msg" => "Não foi possível salvar as configurações."));
        }
    }

    /**
     * Tela de criação de um novo plugin
     */
    public function newAction(){
        $this->loadTemplate("plugin_new", null, "Novo Plugin", array(SITE_URL . 'nyu/adminmedia/js/modules/plugin-new.js'));
    }
    
    /**
     * Cria um novo plugin
     */
    public function newDoAction(){
        if(!$this->post['plugin']){
            $this->json(array('status' => false, 'msg' => 'É necessário informar o id do plugin!'));
        }
        $reserved = array('index', 'list', 'edit', 'save-config', 'new', 'new-do', 'delete', 'install', 'install-do');
        if(in_array($this->post['plugin'], $reserved)){
            $this->json(array('status' => false, 'msg' => 'O id do plugin não pode ser uma das palavras reservadas.'));
        }
        $fm = new \NyuFileManager(\nyuadmin\NyuAdminBase::$adminPluginPath);
        $ret = $fm->createFolder($this->post['plugin']);
        if($ret){
            $this->json(array('status' => true, 'plugin' => $this->post['plugin']));
        }else{
            $this->json(array('status' => false, 'msg' => 'Não foi possível criar o plugin'));
        }
    }
    
    /**
     * Apaga um Plugin
     */
    public function deleteAction(){
        if(!$this->post['plugin']){
            $this->json(array('status' => false, 'msg' => 'É necessário informar o plugin a excluir'));
        }
        $fm = new \NyuFileManager(\nyuadmin\NyuAdminBase::$adminPluginPath);
        $ret = $fm->deleteFolder($this->post['plugin']);
        if($ret){
            $this->json(array('status' => true));
        }else{
            $this->json(array('status' => false, 'msg' => 'Não foi possível excluir o plugin'));
        }
    }
    
    public function installAction(){
        $this->loadTemplate("plugin_install", null, "Instalar Plugin", array(SITE_URL . 'nyu/adminmedia/js/modules/plugin-install.js'));
    }
    
    public function installDoAction(){
        /*echo "<pre>";
        var_dump($_POST);
        var_dump($_FILES);*/
        $plugin = $this->post['plugin'];
        $tempName = $_FILES['zipfile']['tmp_name'];
        $tempNameFolder = pathinfo($_FILES['zipfile']['name'], PATHINFO_FILENAME);
        $reserved = array('index', 'list', 'edit', 'save-config', 'new', 'new-do', 'delete', 'install', 'install-do');
        if(in_array($this->post['plugin'], $reserved)){
            $this->json(array('status' => false, 'msg' => 'O id do plugin não pode ser uma das palavras reservadas.'));
        }
        
        $zip = new ZipArchive();
        if ($zip->open($tempName) === TRUE) {
            // Extrai o arquivo
            $zip->extractTo(\nyuadmin\NyuAdminBase::$adminPluginPath);
            $zip->close();
            // Renomeia a pasta do plugin
            $fm = new \NyuFileManager(\nyuadmin\NyuAdminBase::$adminPluginPath);
            $ret = $fm->rename($tempNameFolder, $plugin);
            $this->json(array('status' => true));
        } else {
            $this->json(array('status' => false, 'msg' => 'Não foi possível instalar o plugin'));
        }
    }
}
