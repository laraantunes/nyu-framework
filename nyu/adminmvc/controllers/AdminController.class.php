<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Classe que extende a controller padrão do sistema, utilizada para adicionar 
 * funcionalidades de template para todo o sistema
 */
class AdminController extends NyuController{
    /**
     * @var NyuTemplate Atributo utilizado para tratar dos templates
     */
    public $template;
    
    /**
     * Sobrescreve o construtor padrão para adicionar a criação do template
     */
    public function __construct(){
        parent::__construct();
        // Cria o objeto template
        $this->template = new NyuTemplate();
    }
    
    /**
     * Método utilizado para carregar o template padrão, já com o cabecalho e 
     * rodapé
     * @param string $template Nome do template (sem a extensão .twig)
     * @param array $vars Array com as variáveis utilizadas na tela
     * @param array $title Título da página
     * @param array $js Array com arquivos js a carregar na página
     * @param array $css Array com arquivos css a carregar na página
     * @param boolean $offline Se true, carrega um template deslogado
     */
    public function loadTemplate($template, $vars=array(), $title=null, $js=array(), $css=array(), $offline=false){
        $vars['_template'] = $template;
        $vars['_js'] = $js;
        $vars['_css'] = $css;
        $vars['_title'] = $title;
        $base_template = "content";
        if($offline){
            $base_template = "offline_content";
        }
        
        // Pega o usuario que está logado atualmente
        $vars['currentUser'] = \nyuadmin\NyuAdminBase::getLoggedUser();
        
        // Registra a classe NyuAdminBase para carregar métodos padrão direto na view
        $adminBase = new \nyuadmin\NyuAdminBase();
        $this->template->addGlobal("AdminBase", $adminBase);
        
        $this->template->renderTemplate("{$base_template}.twig", $vars);
    }
    
    /**
     * Carrega o template com uma mensagem de erro
     * @param string $title Título da mensagem
     * @param string $msg Mensagem
     * @param string $type Tipo da mensagem, para estilização, no padrão do 
     * bootstrap. Valores possíveis:
     * "danger", "info", "warning" e "success"
     * @param boolean $die se true, para a execução do processamento
     */
    public function showMessage($title, $msg, $type = "danger", $die = false){
        $data['type'] = $type;
        $data['title'] = $title;
        $data['msg'] = $msg;
        $this->loadTemplate("message", $data, $title);
        if($die){
            die;
        }
    }
}