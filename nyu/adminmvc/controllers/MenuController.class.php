<?php

class MenuController extends \AdminController{

    public function __construct() {
        parent::__construct();
        // Se não está logado, redireciona para o login
        if(!\nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        if(!\nyuadmin\NyuAdminBase::getLoggedUser()->isAdmin()){
            $this->redirect(NYU_ADMIN_URL);
        }
    }
    
    public function indexAction() {
        $data['list'] = \nyuadmin\MenuTreeItemModel::listBaseMenu();
        $this->loadTemplate("menuitemsbase", $data, "Menus Customizados da Administração", array(SITE_URL.'nyu/adminmedia/js/modules/menu-item-list.js'));
    }
    
    public function newAction(){
        if($this->get[2] == 'child'){
            $data['parent'] = $this->get[3];
            // Carrega o objeto pai para garantir que o objeto existe
            $obj = new \nyuadmin\MenuTreeItemModel();
            $obj->setId($this->get[3]);
            $obj->load();
            if(!$obj->getTitle()){ // O Título é obrigatório, logo, indica que o objeto existe
                $this->showMessage("Não é possível criar o menu filho", "Não foi encontrado um menu pai com o id informado", null, true);
            }
        }
        $this->loadTemplate("menuitem_edit", $data, "Menu Customizado", array(SITE_URL.'nyu/adminmedia/js/modules/menu-item-edit.js'));
    }
    
    public function editAction(){
        if(!$this->get[2]){
            $this->showMessage("Não é possível editar o menu", "É necessário informar um menu a editar", null, true);
        }
        // Carrega o objeto a editar
        $obj = new \nyuadmin\MenuTreeItemModel();
        $obj->setId($this->get[2]);
        $obj->load();
        if(!$obj->getTitle()){ // O Título é obrigatório, logo, indica que o objeto existe
            $this->showMessage("Não é possível editar o menu", "Não foi encontrado um menu com o id informado", null, true);
        }
        $data['obj'] = $obj;
        $data['breadcrumb'] = \nyuadmin\NyuAdminBase::buildBreadCrumb($obj->buildBreadcrumbArray());
        $this->loadTemplate("menuitem_edit", $data, "Menu Customizado", array(SITE_URL.'nyu/adminmedia/js/modules/menu-item-edit.js'));
    }
    
    public function saveAction(){
        $obj = new \nyuadmin\MenuTreeItemModel();
        $obj->setId($this->post['menu_tree_item'] ? $this->post['menu_tree_item'] : null);
        $obj->setTitle($this->post['title'] ? $this->post['title'] : null);
        $obj->setLink($this->post['link'] ? $this->post['link'] : null);
        $obj->setRelative($this->post['relative'] ? true : false);
        $obj->setParent($this->post['parent'] ? $this->post['parent'] : null);
        $obj->setCustomIcon($this->post['custom_icon'] ? $this->post['custom_icon'] : null);
        
        //var_dump($obj);
        if($obj->save()){
            $ret = array('status' => true, 'msg' => 'Menu salvo com sucesso!', 'id' => $obj->getId());
        }else{
            $msg = \NyuCore::getException();
            if(!$msg){
                $msg = 'Não foi possível salvar o menu.';
            }
            $ret = array('status' => false, 'msg' => $msg);
        }
        $this->json($ret);
    }
    
    public function deleteAction(){
        if(!$this->post['menu_tree_item']){
            $this->json(array('status' => false, 'msg' => "ID do menu não informado"));
        }
        $obj = new \nyuadmin\MenuTreeItemModel();
        $c = $obj->count($this->post['menu_tree_item']); //Conta para verificar se a id existe
        if($c == 0){
            $this->json(array('status' => false, 'msg' => "ID do menu não encontrado"));
        }
        $obj->setId($this->post['menu_tree_item']);
        \NyuDb::dbTransaction(); // É necessário abrir uma transação para excluir os menus filhos
        if($obj->delete()){
            \NyuDb::dbCommit(); // Se excluiu corretamente, faz o commit
            $ret = array('status' => true, 'msg' => 'Menu excluído com sucesso!');
        }else{
            \NyuDb::dbRollback(); // Se ocorreu algum erro, faz o rollback
            $msg = \NyuCore::getException();
            if(!$msg){
                $msg = 'Não foi possível excluir o menu.';
            }
            $ret = array('status' => false, 'msg' => $msg);
        }
        $this->json($ret);
    }
}