<?php
/**
 * 2016 Nyu Framework
 */
class UserController extends AdminController{
    
    public function __construct() {
        parent::__construct();
        // Se não existe banco de dados da administração, redireciona para a instalação
        if(!\nyuadmin\NyuAdminBase::nyuAdminInstalled()){
            $this->redirect(NYU_ADMIN_URL."install/");
        }
        
    }
    
    public function indexAction() {
        if(!nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        if(!\nyuadmin\NyuAdminBase::getLoggedUser()->isAdmin()){
            $this->redirect(NYU_ADMIN_URL."user/edit/");
        }
        $data['list'] = \nyuadmin\UserModel::listAll();
        $this->loadTemplate("users", $data, "Usuários", array(SITE_URL.'nyu/adminmedia/js/modules/users.js'));
    }
    
    /**
     * Tela de edição de usuário
     */
    public function editAction(){
        if(!nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        $user = new \nyuadmin\UserModel();
        if($this->get[2]){
            $user->setId($this->get[2]);
            $user->load();
        }
        if(!$this->get[2] && $this->get[1] == 'edit'){
            $user->setId(\nyuadmin\NyuAdminBase::getUserId());
            $user->load();
        }
        $data['obj'] = $user;
        $data['new'] = false;
        if(!$user->getId()){
            $data['new'] = true;
        }
        
        //var_dump(\nyuadmin\NyuAdminBase::getLoggedUser());
        
        $breadcrumb = array(array("title" => "Usuários", "link" => NYU_ADMIN_URL."user/"), array("title" => "Cadastro de Usuário"));
        $data['breadcrumb'] = \nyuadmin\NyuAdminBase::buildBreadCrumb($breadcrumb);

        $this->loadTemplate("user_edit", $data, "Cadastro de Usuário",array(SITE_URL.'nyu/adminmedia/js/modules/user-edit.js'),null);
    }
    
    /**
     * Alias para UserController::editAction() para novos usuários
     */
    public function newAction(){
        $this->editAction();
    }
    
    public function saveAction(){
        $user = new \nyuadmin\UserModel();
        $user->setId($this->post['user']);
        $user->load(); // Carrega o usuário para carregar o dado de administrador ou não e de senha
        $user->setUsername($this->post['username']);
        if($this->post['password']){ // Se enviou a senha, irá editar o campo
            $user->setPassword($this->post['password']);
        }
        $user->setNickname($this->post['nickname']);
        // pega os usuário logado atualmente
        $currentUser = \nyuadmin\NyuAdminBase::getLoggedUser();
        if($currentUser->isAdmin()){ // Se o usuário logado atualmente é administrador, verifica o campo 'admin'
            $user->setAdmin($this->post['admin'] ? true : false);
        }
        $user->setEmail($this->post['email']);
        
        $userBusiness = new \nyuadmin\UserBusiness();
        
        // Executa a gravação e validação do usuário e retorna o json com o resultado
        $userBusiness->run($user, $this);
    }
    
    public function loginAction(){
        $this->loadTemplate("login",null,"Login",array(SITE_URL.'nyu/adminmedia/js/modules/login.js'),null,true);
    }
    
    public function LoginCheckAction(){
        $user = new \nyuadmin\UserModel();
        $user->setUsername($this->post['username']);
        $user->setPassword($this->post['password']);
        if($user->login()){
            // Loga o usuário na sessão
            \nyuadmin\NyuAdminBase::logUserIn($user->getUser());
            $this->json(array("status" => true, 
                              "userId" => $user->getUser(),
                              "msg" => "Usuário logado com sucesso"));
        }else{
            $this->json(array("status" => false, 
                              "msg" => "E-mail ou senha não conferem."));
        }
    }
    
    public function logoffAction(){
        if(!nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        \nyuadmin\NyuAdminBase::logoffUser();
        $this->json(array("status" => true, 
                    "msg" => "Usuário deslogado com sucesso"));
    }
    
    public function testValidationAction(){
        if(!nyuadmin\NyuAdminBase::isUserLogged()){
            $this->redirect(NYU_ADMIN_URL."user/login/");
        }
        $user = new nyuadmin\UserModel();
        \nyuadmin\UserBusiness::run($user, $this);
    }

    public function deleteAction(){
        $user = new \nyuadmin\UserModel();
        $user->setUser($this->post['user']);
        $del = $user->delete();
        if($del){
            $ret = array('status' => true, "msg" => "Usuário excluído com sucesso.");
        }else{
            $msg = \NyuCore::getException();
                if(!$msg){
                    $msg = 'Não foi possível excluir o usuário.';
                }
                $ret = array('status' => false, 'msg' => $msg);
        }
        $this->json($ret);
    }
}



