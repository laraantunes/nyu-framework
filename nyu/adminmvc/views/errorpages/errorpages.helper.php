<?php

 /**
 * Método utilizado para carregar o template de páginas de erro, já com o cabecalho e 
 * rodapé
 * @param string $template Nome do template (sem a extensão .twig)
 * @param array $vars Array com as variáveis utilizadas na tela
 * @param array $title Título da página
 * @param array $js Array com arquivos js a carregar na página
 * @param array $css Array com arquivos css a carregar na página
 */
function loadErrorTemplate($template, $title=null, $vars=array(), $js=array(), $css=array()){
    $vars['_template'] = $template;
    $vars['_js'] = $js;
    $vars['_css'] = $css;
    $vars['_title'] = $title;
    $vars['_link_home_'] = SITE_URL;
    $base_template = "errorpages/error_content";
    $tpl = new NyuTemplate(array('template_dir' => SITE_FOLDER.'/nyu/adminmvc/views'));
    $tpl->renderTemplate("{$base_template}.twig", $vars);
}