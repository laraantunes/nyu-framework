<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Namespace content
 */
namespace content;
/**
 * Classe de metadados de conteúdo
 */
class ContentMetaDataModel extends \nyuadmin\NyuAdminModel{
    protected $contentMetaData;
    protected $contentData;
    protected $key;
    protected $value;
    
    public static $table = "content_metadata";
    public static $cols = array("contentMetaData" => "content_metadata", // PK, Not null
                                "contentData" => "content_data", // FK, Not null
                                "key" => "key", // Not null
                                "value" => "value");

    /**
     * Lista os metadados do conteúdo a partir do id do conteúdo
     */
    public static function listByContentData($contentData){
        return parent::listByKey($contentData, "content_data");
    }

    /**
     * Trigger executada antes de gravar o objeto
     */
    public function triggerBeforeSave() {
        // Se não há id, cria um id aleatório para o objeto
        if(!$this->contentMetaData){
            $this->contentMetaData = \nyuadmin\NyuAdminBase::generateId("contentMetaData");
        }

        if(!$this->contentData){
            throw new \Exception("É necessário vincular o metadado à um conteúdo");
        }

        if(!$this->key){
            throw new \Exception("É necessário indicar uma chave para o metadado");
        }
    }
}