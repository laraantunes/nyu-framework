<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Namespace content
 */
namespace content;
/**
 * Classe de conteúdo dinâmico
 */
class ContentDataModel extends \nyuadmin\NyuAdminModel{
    protected $contentData;
    protected $identifier;
    protected $title;
    protected $description;
    protected $content;
    protected $date;
    protected $parent;
    protected $type;
    
    public static $table = "content_data";
    public static $cols = array("contentData" => "content_data", // PK, Not null
                                "identifier" => "identifier",
                                "title" => "title",
                                "description" => "description",
                                "content" => "content",
                                "date" => "date",
                                "parent" => "parent",
                                "type" => "type");
    
    /* Getters and Setters */
    
    public function getContentData() {
        return $this->contentData;
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getContent() {
        return $this->content;
    }

    public function getDate() {
        return $this->date;
    }

    public function getParent() {
        return $this->parent;
    }

    public function getType() {
        return $this->type;
    }

    public function setContentData($contentData) {
        $this->contentData = $contentData;
    }

    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setContent($content) {
        $this->content = $content;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setParent($parent) {
        $this->parent = $parent;
    }

    public function setType($type) {
        $this->type = $type;
    }
                            
    /**
     * Trigger executada antes de gravar o objeto
     */
    public function triggerBeforeSave() {
        // Se não há id, cria um id aleatório para o objeto
        if(!$this->contentData){
            $this->contentData = \nyuadmin\NyuAdminBase::generateId("contentData");
        }
        // Se não há data de cadastro, cria uma data
        if(!$this->date){
        	$this->date = date('Y-m-d H:i:s');
        }
    }
    
    /**
     * Carrega o conteúdo por um identificador
     * @param string $identifier Identificador fixo do conteúdo em específico.
     * Como exemplo: Um conteúdo base (blocos de uma página, por exemplo) pode
     * ter um identificador fixo para que seja identificado mais facilmente
     * em buscas, como "contact-form" para o bloco do formulário de contato.
     * @return ContentDataModel O objeto carregado
     */
    public function loadByIdentifier($identifier = null){
        if($identifier){
            $this->identifier = $identifier;
        }
    	return $this->load("identifier");
    }

    /**
     * Carrega os filhos do objeto atual
     * @return array Lista dos ContentDataModel filhos do conteúdo atual
     */
    public function children(){
    	return parent::listByKey($this->contentData, "parent");
    }
    
    /**
     * Se contém filhos, retorna true, se não, retorna false
     * @return boolean
     */
    public function hasChildren(){
        $count = $this->count($this->contentData, "parent");
        if($count > 0){
            return true;
        }
        return false;
    }
    
    public function getChildrenByIdentifier($identifier, $index = null){
        $list = parent::listAll(array("date"), array("parent = ?" => $this->parent, "and identifier = ?" => $identifier));
        if(!$list){
            return false;
        }
        if($index != null){
            return $list[$index];
        }
        return $list;
    }

    /**
     * Carrega os metadados do objeto atual
     * @return array Lista dos ContentMetadataModel relacionados ao conteúdo atual
     */
    public function metadata(){
    	return \nyucontent\ContentMetaDataModel::listByContentData($this->contentData);
    }
    
    /**
     * Lista os conteúdos base (blocos de uma página, por exemplo. Conteúdos base
     * são conteúdos que agrupam outros conteúdos, são utilizados para que seja
     * possível uma manutenção mais simples e organizada do conteúdo)
     */
    public static function listBaseContents(){
        return parent::listAll(array('title'), "parent is null");
    }
}