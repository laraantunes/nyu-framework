<?php
namespace nyuadmin;
class UserModel extends NyuAdminModel{
    protected $user;
    protected $username;
    protected $nickname;
    protected $password;
    protected $admin;
    protected $email;
    
    public static $table = "user";
    public static $cols = array("user" => "user", 
                                "username" => "username",
                                "nickname" => "nickname",
                                "password" => "password",
                                "admin" => "admin",
                                "email" => "email");
    
    public function getUser() {
        return $this->user;
    }
    
    /**
     * Alias para getUser()
     * @return int
     */
    public function getId(){
        return $this->getUser();
    }

    public function getUsername() {
        return $this->username;
    }

    public function getNickname() {
        return $this->nickname;
    }

    public function getPassword() {
        return $this->password;
    }
    
    public function getAdmin() {
        return $this->admin;
    }
    
    /**
     * Retorna se o usuário é administrador
     * @return boolean
     */
    public function isAdmin() {
        return $this->admin ? true : false;;
    }
    
    public function getEmail() {
        return $this->email;
    }

    public function setUser($user) {
        $this->user = $user;
        return $this;
    }
    
    public function setId($user){
        return $this->setUser($user);
    }

    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    public function setNickname($nickname) {
        $this->nickname = $nickname;
        return $this;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }
    
    public function setAdmin($admin) {
        if(is_bool($admin)){
            $this->admin = $admin ? 1 : null;
        }else{
            $this->admin = $admin;
        }
        return $this;
    }
    
    public function setEmail($email) {
        $this->email = $email;
    }
    
    /**
     * Encoda a senha do usuario
     * @param boolean $onlyEncode Se true, irá apenas encodar e retornar, não 
     * alterando o valor da senha no objeto
     * @return null|string
     */
    public function encodePassword($onlyEncode = false){
        $password = sha1($this->password.NYU_ADMIN_SALT);
        if($onlyEncode){
            return $password;
        }
        $this->password = $password;
        return $this->password;
    }

    public function triggerBeforeSave() {
        // Se não há id, cria um id aleatório para o objeto
        if(!$this->user){
            $this->user = \nyuadmin\NyuAdminBase::generateId("user");
        }
        if($this->password){
            $this->encodePassword();
        }else{
            throw new \Exception("É necessário informar uma senha");
        }        
        if(!$this->username){
            throw new \Exception("É necessário informar um nome de usuário");
        }
    }
    
    /**
     * Tenta fazer o login de usuário
     */
    public function login(){
        // Pega a instância do banco de dados
        $db = \NyuDb::getInstance();
        $sql = "select user from user where username = ? and password = ?";
        $bind = array($this->username, $this->encodePassword(true));
        $ret = $db->query($sql, $bind);
        if($ret){
            $this->user = $ret[0]['user'];
            return $this->user;
        }else{
            return false;
        }
    }
}