<?php

namespace nyuadmin;
class MenuTreeItemModel extends NyuAdminModel{
    protected $menuTreeItem;
    protected $title;
    protected $link;
    protected $relative;
    protected $parent;
    protected $prefix;
    protected $customIcon;
    
    public static $table = "menu_tree_item";
    public static $cols = array("menuTreeItem" => "menu_tree_item", 
                                "title" => "title",
                                "link" => "link",
                                "relative" => "relative",
                                "parent" => "parent",
                                "customIcon" => "custom_icon");
    
    /**
     * Construtor da classe MenuTreeItemModel
     */
    public function __construct() {
        // Força o prefixo padrão
        $this->prefix = NYU_ADMIN_URL;
        // Força por padrão ser um link relativo
        $this->setRelative(true);
    }
    
    /* Getters and Setters */
    
    public function getMenuTreeItem() {
        return $this->menuTreeItem;
    }
    
    /**
     * Alias para getMenuTreeItem
     * @return string id do objeto atual
     */
    public function getId($menuTreeItem) {
        return $this->getMenuTreeItem();
    }

    public function getTitle() {
        return $this->title;
    }

    public function getLink() {
        return $this->link;
    }
    
    /**
     * Retorna o link completo, incluindo o atributo prefix
     * @return string o link completo
     */
    public function getFullLink(){
        if($this->isRelative()){
            $prefix = trim($this->prefix, "/");
            $link = ltrim($this->link, "/");
            return $prefix . "/" . $link;
        }
        return $this->link;
    }

    public function getParent() {
        return $this->parent;
    }
    
    public function getPrefix() {
        return $this->prefix;
    }

    public function getCustomIcon(){
        return $this->customIcon;
    }

    public function setMenuTreeItem($menuTreeItem) {
        $this->menuTreeItem = $menuTreeItem;
        return $this;
    }
    
    /**
     * Alias para setMenuTreeItem
     * @param string $menuTreeItem
     * @return MenuTreeItemModel o objeto atual
     */
    public function setId($menuTreeItem) {
        return $this->setMenuTreeItem($menuTreeItem);
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function setLink($link) {
        $this->link = $link;
        return $this;
    }

    public function setParent($parent) {
        $this->parent = $parent;
        return $this;
    }
    
    public function setPrefix($prefix) {
        $this->prefix = $prefix;
        return $this;
    }

    public function setCustomIcon($customIcon){
        $this->customIcon = $customIcon;
        return $this;
    }
    
    /**
     * Seta se o link é relativo ou não
     * @param boolean|integer $relative
     */
    public function setRelative($relative){
        if(is_bool($relative)){
            $this->relative = $relative ? 1 : null;
        }else{
            $this->relative = $relative;
        }
        return $this;
    }
    
    /**
     * Verifica se o link é relativo
     * @return integer
     */
    public function getRelative(){
        return $this->relative;
    }
    
    /**
     * Verifica se o link é relativo
     * @return boolean
     */
    public function isRelative(){
        return $this->relative ? true : false;
    }
    
    public function triggerBeforeSave() {
        // Se não há id, cria um id aleatório para o objeto
        if(!$this->menuTreeItem){
            $this->menuTreeItem = \nyuadmin\NyuAdminBase::generateId("menuTreeItem");
        }
        
        if(!$this->title){
            throw new \Exception("É necessário informar um título para o menu.");
        }
    }
    
    public function triggerBeforeDelete() {
        if($this->hasChildren()){ // Se possui filhos, exclui os filhos
            foreach($this->children() as $child){
                $child->delete();
            }
        }
    }

    /**
     * Carrega os filhos do objeto atual
     * @return array Lista dos MenuTreeItem filhos do conteúdo atual
     */
    public function children(){
    	return parent::listByKey($this->menuTreeItem, "parent", array("title"));
    }

    /**
     * Se contém filhos, retorna true, se não, retorna false
     * @return boolean
     */
    public function hasChildren(){
        $count = $this->count($this->menuTreeItem, "parent");
        if($count > 0){
            return true;
        }
        return false;
    }
    
    /**
     * Lista os Ítens base do menu (itens sem pai)
     */
    public static function listBaseMenu(){
        return parent::listAll(array('title'), "parent is null");
    }
    
    /**
     * Retorna o array com os pais para montar a breadcrumb
     * @param boolean $main Indica que é a primeira chamada
     * @return array
     */
    public function buildBreadcrumbArray($main = true){
        $a = array();
        $a[] = array('title' => $this->title, 
                     'link' => NYU_ADMIN_URL."menu/edit/".$this->menuTreeItem);
        if($this->parent){ // Se possui pai, busca para adicionar no array
            $parent = new self();
            $parent->setId($this->parent);
            $parent->load();
            $a = array_merge($a, $parent->buildBreadcrumbArray(false));
        }else{
            $a[] = array('title' => "Menus Customizados", 
                     'link' => NYU_ADMIN_URL."menu/");
        }
        if($main){
            $a = array_reverse($a);
        }
        return $a;
    }
}