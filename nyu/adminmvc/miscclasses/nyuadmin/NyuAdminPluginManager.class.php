<?php

/**
 * 2016 Nyu Framework
 */
/**
 * Namespace nyuadmin
 */

namespace nyuadmin;

/**
 * Classe que gerencia os plugins da administração do Nyu
 * @version 1.0
 */
class NyuAdminPluginManager {

    /**
     * Nome do plugin
     * @var string
     */
    protected $plugin;

    /**
     * Objeto NyuFileManager que irá tratar dos arquivos do plugin
     * @var \NyuFileManager
     */
    protected $fm;

    /**
     * Array com as configurações do plugin
     * @var array
     */
    protected $config;
    
    /**
     * Caminho do arquivo de configurações do plugin
     * @var string
     */
    protected $configPath;

    /**
     * Construtor da classe NyuAdminPluginManager
     * @param string $plugin nome do plugin - nome da pasta onde está o plugin
     */
    public function __construct($plugin) {
        $this->plugin = $plugin;
        $this->fm = new \NyuFileManager(NyuAdminBase::$adminPluginPath);
        if ($this->pluginExists()) {
            $this->configPath = $this->plugin . DS . 'config.json';
            $this->config = $this->getPluginConfig();
            
        }
    }

    /**
     * Verifica se o plugin existe
     * @return boolean true se existe, false se não
     */
    public function pluginExists() {
        return $this->fm->fileExists($this->plugin);
    }

    /**
     * Retorna as configurações do plugin
     * @param boolean $convertToArray Se true, irá converter o conteúdo do arquivo json em array
     * @return array configurações do plugin, salvas no arquivo 'config.json' dentro da pasta do plugin
     */
    public function getPluginConfig($convertToArray = true) {
        if($convertToArray){
            return $this->fm->loadJsonFile($this->configPath);
        }else{
            return $this->fm->loadFile($this->configPath);
        }
    }
    
    /**
     * Retorna o caminho do plugin
     * @return string
     */
    public function getPluginPath(){
        return NyuAdminBase::$adminPluginPath.$this->plugin;
    }
    
    /**
     * Retorna o caminho do arquivo de configurações do plugin
     * @return string
     */
    public function getPluginConfigPath(){
        return $this->configPath;
    }
    
    /**
     * Altera o conteúdo do arquivo de configurações do plugin
     * @param string $newConfig O novo conteúdo do arquivo de configurações
     * @return boolean
     */
    public function savePluginConfigFile($newConfig){
        return $this->fm->saveFile($this->configPath, $newConfig);
    }

    /**
     * Carrega a pasta de controllers do plugin
     * @return \NyuAdminPluginManager O objeto atual
     */
    public function loadPluginControllersFolder() {
        $this->fm->autoloadFolder($this->plugin . DS . (isset($this->config['controllers_folder']) ? $this->config['controllers_folder'] : 'controllers'));
        return $this;
    }

    /**
     * Carrega a pasta de models do plugin
     * @return \NyuAdminPluginManager O objeto atual
     */
    public function loadPluginModelsFolder() {
        $this->fm->autoloadFolder($this->plugin . DS . (isset($this->config['models_folder']) ? $this->config['models_folder'] : 'models'));
        return $this;
    }

    /**
     * Retorna a pasta em que estão as views do plugin
     * @return string
     */
    public function getPluginViewsFolder() {
        return $this->fm->getPath() . ($this->plugin . DS . (isset($this->config['views_folder']) ? $this->config['views_folder'] : 'views')) . DS;
    }

    /**
     * Retorna uma configuração do plugin
     * @return mixed
     */
    public function getConfig($config) {
        return $this->config[$config];
    }

    /**
     * Lista os plugins instalados
     */
    public static function listPlugins() {
        $fm = new \NyuFileManager(NyuAdminBase::$adminPluginPath);
        $folders = $fm->getFolders();
        array_shift($folders);
        return $folders;
    }

}
