<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Namespace nyuadmin
 */
namespace nyuadmin;
//error_reporting(E_ALL);
//ini_set("display_errors", true);
/**
 * Carrega a classe de elementos para montar htmls dinamicamente
 */ 
use \nyuxmlgen\Element as El;
/**
 * Caminho para a pasta de dados da administração
 */
define('admindataPath', SITE_FOLDER."/nyu/admindata/");
/**
 * Caminho para a pasta de plugins da administração
 */
define('adminPluginPath', SITE_FOLDER."/nyu/adminmvc/plugins/");
/**
 * Classe Base para a instalação da administração do Nyu
 * @version 1.0
 */
class NyuAdminBase{
    /**
     * Caminho para a pasta de plugins da administração
     * @var string
     */
    public static $adminPluginPath = \adminPluginPath;
    /**
     * Caminho para a pasta de dados da administração
     * @var string
     */
    public static $admindataPath = \admindataPath;
    /**
     * Nome do arquivo de banco de dados da administração
     * @var string
     */
    public static $databaseName = "nyuadmin.sqlite";
    
    /**
     * Identificador do id do usuário na sessão
     * @var string
     */
    public static $sessionUserIdVarName = "_nyuadmin_userid";
    
    /**
     * Nome do arquivo com informações do banco de dados customizado
     * @var string
     */
    public static $customDbDataFileName = "customdatabase.nyudata";
    
    /*
     * Constantes de criação de tabelas de banco de dados
     */
    /**
     * Tabela user
     */
    const database_create_table_user = ' CREATE TABLE user (user varchar(100) PRIMARY KEY NOT NULL, username varchar(100) NOT NULL UNIQUE, nickname varchar(100) , password varchar(255) NOT NULL, admin INT, email varchar(255) ) ';
    /**
     * Tabela content_data
     */
    const database_create_table_content_data_sqlite = ' CREATE TABLE content_data (content_data VARCHAR (100) PRIMARY KEY NOT NULL, identifier VARCHAR (255), title VARCHAR (255), description TEXT, content TEXT, date DATETIME, parent VARCHAR (100) CONSTRAINT fk_content_data_parent REFERENCES content_data (content_data), type VARCHAR (100)) ';
    const database_create_table_content_data_mysql = ' CREATE TABLE `content_data` (`content_data` VARCHAR(100) NOT NULL, `identifier` VARCHAR(255) NULL DEFAULT NULL, `title` VARCHAR(255) NULL DEFAULT NULL, `description` TEXT NULL, `content` TEXT NULL, `date` TIMESTAMP NULL DEFAULT NULL, `parent` VARCHAR(100) NULL DEFAULT NULL, `type` VARCHAR(100) NULL DEFAULT NULL, PRIMARY KEY (`content_data`), INDEX `fk_content_data_parent` (`parent`), CONSTRAINT `fk_content_data_parent` FOREIGN KEY (`parent`) REFERENCES `content_data` (`content_data`)) ';
    /**
     * Tabela content_metadata
     */
    const database_create_table_content_metadata_sqlite = ' CREATE TABLE content_metadata (content_metadata VARCHAR (100) PRIMARY KEY NOT NULL, content_data VARCHAR (100) CONSTRAINT fk_content_metadata_content_data REFERENCES content_data (content_data) NOT NULL, [key] VARCHAR (255) NOT NULL, value TEXT) ';
    const database_create_table_content_metadata_mysql = ' CREATE TABLE `content_metadata` (`content_metadata` VARCHAR(100) NOT NULL, `content_data` VARCHAR(100) NOT NULL, `key` VARCHAR(255) NOT NULL, `value` TEXT NULL, PRIMARY KEY (`content_metadata`), INDEX `fk_content_metadata_content_data` (`content_data`), CONSTRAINT `fk_content_metadata_content_data` FOREIGN KEY (`content_data`) REFERENCES `content_data` (`content_data`)) ';
    /**
     * Tabela menu_tree_item
     */
    const database_create_table_menu_tree_item_sqlite = ' CREATE TABLE menu_tree_item (menu_tree_item VARCHAR (100) PRIMARY KEY NOT NULL, title VARCHAR (255) NOT NULL, link TEXT, relative INT, custom_icon VARCHAR (255), parent VARCHAR (100) CONSTRAINT fk_menu_tree_item_parent REFERENCES menu_tree_item (menu_tree_item)) ';
    const database_create_table_menu_tree_item_mysql = ' CREATE TABLE `menu_tree_item` (`menu_tree_item` VARCHAR(100) NOT NULL, `title` VARCHAR(255) NOT NULL, `link` TEXT NULL, `relative` INT(11) NULL DEFAULT NULL, custom_icon VARCHAR (255) NULL DEFAULT NULL, `parent` VARCHAR(100) NULL DEFAULT NULL, PRIMARY KEY (`menu_tree_item`), INDEX `fk_menu_tree_item_parent` (`parent`), CONSTRAINT `fk_menu_tree_item_parent` FOREIGN KEY (`parent`) REFERENCES `menu_tree_item` (`menu_tree_item`)) ';

    /**
     * Instala a administração do sistema
     */
    public static function install($customdata = false){
        if($customdata && $customdata['databasedriver'] != 'sqlite'){
            $return = self::generateDatabaseMySQL($customdata);
        }else{
            $return = self::generateDatabaseSQLite();
        }
        if($return['status']){
            // Exclui o arquivo temporario de instalação
            $fm = new \NyuFileManager(SITE_FOLDER);
            $fm->deleteFile('nyu-install');
        }
        return $return;
    }
    
    /**
     * Método que cria o banco de dados padrão para a administração do sistema no padrão SQLite
     */
    public static function generateDatabaseSQLite(){
        if(!self::databaseExists()){
            // Cria o arquivo banco de dados
            $fm = new \NyuFileManager(self::$admindataPath);
            $fm->saveFile(self::$databaseName, "");
            // Cria as tabelas padrão
            try{
                $db = new \NyuDb();
                // Sqlite executa na mesma instrução todos os comandos create table
                $db->quickexec(self::database_create_table_user.";".
                        self::database_create_table_content_data_sqlite.";".
                        self::database_create_table_content_metadata_sqlite.";".
                        self::database_create_table_menu_tree_item_sqlite.";");
            }catch(\Exception $e){
                $error = $e->getMessage();
            }
            if(!$error){
                $error = \NyuCore::getException();
            }if(!$error){
                return array("status"=>true,"msg"=>'banco de dados criado e configurado com sucesso.');
            }else{
                return array("status"=>false,"msg"=>$error);
            }
        }else{
            return array("status"=>false,"msg"=>'banco de dados já existente.');
        }
    }
    
        /**
     * Método que cria o banco de dados padrão para a administração do sistema no padrão MySQL
     */
    public static function generateDatabaseMySQL($customdata){
        $nyu__database_config = '_nyu_admin_';
        
        $databaseConfig = array("driver" => "mysql",
                           "host" => $customdata['host'],
                           "name" => $customdata['database'],
                           "user" => $customdata['username'],
                           "password" => $customdata['password']);
        if($customdata['port']){
            $databaseConfig['port'] = $customdata['port'];
        }
        
        \NyuConfig::addConfig("database",
                     "_nyu_admin_",
                     $databaseConfig);
        
        try{
            // Cria as tabelas padrão
            $db = new \NyuDb();
            $db->manualCommit();
            // Mysql deve passar false no autocommit, para fazer o commit somente após executar todas as instruções
            $db->quickexec(self::database_create_table_user, false);
            $db->quickexec(self::database_create_table_content_data_mysql, false);
            $db->quickexec(self::database_create_table_content_metadata_mysql, false);
            $db->quickexec(self::database_create_table_menu_tree_item_mysql, false);
            $db->commit();
        }catch(\Exception $e){
            if($db){
                $db->rollback();
            }
            $error = $e->getMessage();
        }
        // Se ocorreu algum erro em um outro processamento
        if(!$error){
            $error = \NyuCore::getException();
        }
        if(!$error){
            $fm = new \NyuFileManager(self::$admindataPath);
            $fm->saveJsonFile(self::$customDbDataFileName, $databaseConfig);
            return array("status"=>true,"msg"=>'banco de dados criado e configurado com sucesso.');
        }else{
            return array("status"=>false,"msg"=> $error);
        }
    }
    
    /**
     * Verifica se o banco de dados da administração existe
     * @return boolean
     */
    public static function databaseExists(){
        $fm = new \NyuFileManager(self::$admindataPath);
        // Se existe um arquivo de configurações do banco de dados customizado
        if($fm->fileExists(self::$customDbDataFileName)){
            return true;
        }
        return $fm->fileExists(self::$databaseName);
    }
    
    /**
     * Verifica se existe um usuário no banco de dados da administração
     * @return boolean
     */
    public static function theresSomeUser(){
        if(!self::databaseExists()){
            return false;
        }
        $db = new \NyuDb();
        $c = $db->count("user");
        if($c > 0){
            return true;
        }
        return false;
    }
    
    /**
     * Gera uma id aleatória
     * @param string $value um valor para adicionar na chave
     * @return string
     */
    public static function generateId($value=null){
        $id = sha1(rand(0,9999).$value.time().NYU_ADMIN_SALT.rand(0,9999).date('dmY'));
        return $id;
    }
    
    /**
     * Verifica se a administração do nyu foi instalada
     * @return boolean
     */
    public static function nyuAdminInstalled(){
        if(self::databaseExists() && self::theresSomeUser()){
            return true;
        }
        return false;
    }
    
    /**
     * Verifica se há um usuario logado na administração do nyu
     * @return boolean
     */
    public static function isUserLogged(){
        $userid = \NyuCore::loadFromSess(self::$sessionUserIdVarName, false);
        if($userid){
            return true;
        }
        return false;
    }
    
    /**
     * Loga um usuário na sessão
     * @param number $id
     */
    public static function logUserIn($id){
        \NyuCore::saveInSess(self::$sessionUserIdVarName, $id);
    }
    
    /**
     * Retorna o id do usuário logado
     * @return boolean|number retorna falso em caso de usuario nao logado
     */
    public static function getUserId(){
        if(self::isUserLogged()){
            return \NyuCore::loadFromSess(self::$sessionUserIdVarName, false);
        }
        return false;
    }
    
    /**
     * Retorna o objeto do usuário logado
     * @return \nyuadmin\UserModel|boolean
     */
    public static function getLoggedUser(){
        $userId = self::getUserId();
        if(!$userId){
            return false;
        }
        $user = new \nyuadmin\UserModel();
        $user->setUser($userId);
        $user->load();
        return $user;
    }
    
    /**
     * Desloga o usuário
     */
    public static function logoffUser(){
        \NyuCore::deleteFromSess(self::$sessionUserIdVarName);
    }

    /**
     * Constrói o menu recursivamente
     * @return array Array de elementos construído
     */
    public static function __buildMenuElements($list = false){
        // Se não é filho, carrega a lista base
        if(!$list){
            $list = \nyuadmin\MenuTreeItemModel::listBaseMenu();
        }

        foreach($list as $obj){
            $el = new El("li");
            $elA = new El("a");
            $elA->addAttr("href", ($obj->getLink() ? ($obj->isRelative() ? NYU_ADMIN_URL.$obj->getLink() : $obj->getLink()) : "#"));
            $elI = new El("i");
            $elI->addAttr("class", ($obj->getCustomIcon() ? $obj->getCustomIcon() : "fa fa-circle-o"));
            $elI->setOpenTag(true);
            $elA->addChild($elI);
            if($obj->hasChildren()){
                $elA->addChild(new El("span",$obj->getTitle()));
            }else{
                $elA->setValue(" ".$obj->getTitle());
            }
            if($obj->hasChildren()){
                $arrow = new El('i');
                $arrow->addAttr("class", "fa fa-angle-left pull-right");
                $arrow->setOpenTag(true);
                $elA->addChild($arrow);
            }
            $el->addChild($elA);
            if($obj->hasChildren()){
                $el->addAttr("class", "treeview");
                $ul = new El("ul");
                $ul->addAttr("class", "treeview-menu");
                // Monta recursivamente as <li>s
                $children = self::__buildMenuElements($obj->children());
                foreach($children as $child){
                    $ul->addChild($child);
                }
                $el->addChild($ul);
            }
            $ret[] = $el;
        }
        return $ret;
    }
    
    /**
     * Constrói o menu customizado
     * @return string o HTML do menu construído
     */
    public static function buildMenu(){
        $list = self::__buildMenuElements();
        $html = "";
        if($list){
            foreach($list as $li){
                $html .= $li;
            }
        }
        return $html;
    }
    
    /**
     * Monta uma breadcrumb a partir de um array
     * @param array $list Lista de valores para montar a breadcrumb no seguinte 
     * padrão: ['link'] => <link que será inserido no element>
     * ['title'] => <título que será exibido na breadcrumb>
     */
    public static function buildBreadCrumb($list){
        $ol = new El("ol");
        $ol->addAttr("class", "breadcrumb");
        $first = true;
        foreach($list as $item){
            $li = new El("li");
            $a = new El("a");
            $a->addAttr("href", $item['link']);
            if($first){
                $i = new El("i");
                $i->addAttr("class", "fa fa-map-marker");
                $i->setOpenTag(true);
                $a->addChild($i);
                $a->setValue(" ".$item['title']); // Se for o primeiro elemento, precisa de um espaço
                $first = false;
            }else{
                $a->setValue($item['title']); // Outros elementos
            }
            $li->addChild($a);
            $ol->addChild($li);
        }
        $li->addAttr("class", "active"); // Aplica a classe "active" no último elemento
        $li->setValue($a->getValue()); // Seta o valor do último elemento
        $li->removeChild($a); // Remove o link do último elemento
        $a->removeAttr("href"); // Remove o href do ultimo elemento
        return $ol;
    }
}