<?php
/**
 * 2016 Nyu Framework
 */
/**
 * Namespace nyuadmin
 */
namespace nyuadmin;

//error_reporting(E_ALL);
//ini_set('display_errors', true);
/**
 * Classe que gerencia as regras de negócio do Usuário
 */
class UserBusiness extends \NyuBusiness{
    
    /**
     * Trigger executada antes da validação
     */
    public function triggerBeforeValidate() {
        if($this->getObject()->getId() && !$this->getObject()->getPassword()){
           // Se já existe o usuario e não editou a senha, carrega a senha original para não gravar vazio
           $usertmp = new \nyuadmin\UserModel();
           $usertmp->setId($this->getObject()->getId());
           $usertmp->load();
           $this->getObject()->setId($usertmp->getPassword());
        }
    }

    /**
     * Executa a regra de negócio
     * @param Object $object objeto a ser validado
     * @param \NyuController $controller controller atual
     */
    public static function run($object = null, $controller = null) {
        $obj = parent::run($object, $controller);
        $obj->setRules(array(array('attr' => 'password', 'min_length' => 6, 'message' => 'A senha deve conter ao menos 6 caracteres')));
        //$obj->setRules(array(array('attr' => 'username', 'required' => true, 'fieldName' => 'Nome de usuário')));

        try{
            if($obj->save()){
                $ret = array('status' => true, 'msg' => 'Usuário salvo com sucesso!', 'id' => $object->getId());
            }else{
                $msg = \NyuCore::getException();
                if(!$msg){
                    $msg = 'Não foi possível salvar o usuário.';
                }
                $ret = array('status' => false, 'msg' => $msg);
            }
        }catch(\NyuBusinessException $e){
            $ret = array('status' => false, 'msg' => $e->getMessage());
        }catch(Exception $e){
            $ret = array('status' => false, 'msg' => $e->getMessage());
        }
        
        $controller->json($ret);
    }
}