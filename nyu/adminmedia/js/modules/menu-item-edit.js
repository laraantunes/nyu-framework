function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

$(function(){
    // Regras do validate no formulario
    $('#nyu_admin_menu_form').validate({
        rules: {
            title: {required: true}
        },
        messages: {
            title: {required: 'É necessário informar um título para o menu'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            var dados = $("#nyu_admin_menu_form").serialize();
            $.ajax({
                type: "POST",
                url: $("#nyu_admin_menu_form").attr('action'),
                data: dados,
                dataType: 'json',
                success: function (data){
                    if(data.status){
                        $('#menu_tree_item').val(data.id);
                        $('#newchild').show();
                        if($('#newchild').attr('data-new') == 'true'){
                            $('#newchild').attr('href', $('#newchild').attr('href')+data.id+'/');
                        }
                        $('#newchild').attr('data-new', 'false');
                        $('.breadcrumb .active').html($('#title').val());
                    }
                    modalMensagem(data.msg);
                }
            });
            return false;
        }
    });
    
    // Exclusão
    $(".menu-delete").click(function(){
        title = $(this).attr('data-title');
        if(confirm("Confirma a exclusão do menu "+title+"? Todos os menus filhos serão excluídos.")){
            id = $(this).attr('data-id');
            $.ajax({
                    type: "POST",
                    url: NYU_ADMIN_URL+'menu/delete/',
                    data: {'menu_tree_item' : id},
                    dataType: 'json',
                    success: function (data){
                        if(data.status){
                            if(id == $('#menu_tree_item').val()){
                                if($('#parent').val() != null && $('#parent').val() != ''){
                                    window.location.href = NYU_ADMIN_URL + 'menu/edit/'+$('#parent').val();
                                }else{
                                    window.location.href = NYU_ADMIN_URL + 'menu';
                                }
                            }else{
                                window.location.href = window.location.href;
                            }
                        }else{
                                modalMensagem(data.msg);
                        }
                    }
            });
        }
    });
});