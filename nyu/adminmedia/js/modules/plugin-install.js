$(function(){
    // Regras do validate no formulario
    $('#plugin_install_form').validate({
        rules: {
            plugin: {required: true}
        },
        messages: {
            plugin: {required: 'É necessário informar o Id do plugin'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            //var dados = $("#plugin_install_form").serialize();

            $.ajax({
                type: "POST",
                url: $('#plugin_install_form').attr('action'),
                data: new FormData($('#plugin_install_form')[0]),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                success: function (data){
                    if(data.status){
                        document.location.href = NYU_ADMIN_URL + 'plugin/list/';
                    }else{
                        modalMensagem(data.msg);
                    }
                }
            });
            return false;
        }
    });
    
    $("#zipfile").change(function () {
        var file = this.files[0];
        var fileType = file.type;
        var match = ["application/zip"];
        if (fileType != match[0]){
            modalMensagem("É necessário informar um arquivo Zip!");
            return false;
        }
    });
});