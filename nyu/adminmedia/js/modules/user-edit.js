function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

$(function(){
    function getRidOffAutocomplete(){
        var timer = window.setTimeout( function(){
            $('#password').prop('disabled',false);
            clearTimeout(timer);
           },
        800);
    }

    // Invoke the function, handle page load autocomplete by chrome.
    getRidOffAutocomplete();
    
    
    // Regras do validate no formulario
    $('#nyu_admin_user_form').validate({
        rules: {
            username: {required: true}
        },
        messages: {
            username: {required: 'É necessário informar o nome do usuário'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            var dados = $("#nyu_admin_user_form").serialize();
            $.ajax({
                type: "POST",
                url: $('#nyu_admin_user_form').attr('action'),
                data: dados,
                dataType: 'json',
                success: function (data){
                    if(data.status){
                        $('#user').val(data.id);
                    }
                    modalMensagem(data.msg);
                }
            });
            return false;
        }
    });
    
    $('#username').keydown(function (e) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 13 || e.keyCode == 9 || 
            e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || 
            e.keyCode == 20 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 45 || 
            e.shiftKey || e.altKey || e.ctrlKey){
            return true;
        }
        var regex = new RegExp("^[a-zA-Z0-9\.\_]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    
    $('#nickname').keydown(function (e) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 13 || e.keyCode == 9 || 
            e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || 
            e.keyCode == 20 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 45 || 
            e.shiftKey || e.altKey || e.ctrlKey){
            return true;
        }
        var accentedCharacters = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ";
        var regex = new RegExp("^[a-zA-Z0-9"+accentedCharacters+"\.\_]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
});