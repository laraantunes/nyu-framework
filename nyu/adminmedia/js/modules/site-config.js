var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/php");

$(function(){
    $("#save-content").click(function(){
        var code = editor.getValue();
        $.ajax({
            type: "POST",
            url: NYU_ADMIN_URL+'config/save-site/',
            data: {'content' : code},
            dataType: 'json',
            success: function (data){
                modalMensagem(data.msg);
            }
        });
    });
    
    $("#refresh").click(function(){
        window.location.href = window.location.href;
    });
});