function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $('input').on('ifChecked', function (event) {
        if ($("#database-mysql").is(':checked')) {
            $("#mysql-config").show();
        }else{
            $("#mysql-config").hide();
        }
    });
    
    $('#nyu_admin_install_custom').validate({
        rules: {
            host: {required: true},
            database: {required: true},
            username: {required: true}
        },
        messages: {
            host: {required: 'É necessário informar o endereço do banco de dados'},
            database: {required: 'É necessário informar o nome do banco de dados'},
            username: {required: 'É necessário informar o nome do usuário'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            var dados = $("#nyu_admin_install_custom").serialize();
            $.ajax({
                type: "POST",
                url: $("#nyu_admin_install_custom").attr('action'),
                data: dados,
                dataType: 'json',
                success: function (data){
                    if(data.status){
                        window.location.href = NYU_ADMIN_URL+'install/user/';
                    }else{
                        modalMensagem(data.msg);
                    }
                }
            });
            return false;
        }
    });
});