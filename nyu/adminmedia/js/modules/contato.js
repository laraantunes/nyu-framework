/**
 * Métodos Javascript da tela de contato
 */

// Funções utilizadas na tela
/**
 * Carrega as cidades a partir do estado selecionado
 * @param {int} cidadeSelecionada Se informado, seleciona a cidade cujo id é 
 * o enviado
 */
function carregaCidades(cidadeSelecionada) {
    // Carrega as Cidades
    $.ajax({
        type: 'POST',
        url: "contato/get-cidades",
        data: {estado: $('#estadoId option:selected').val()},
        success: function (data) {
            // Limpa a combo de cidades
            $('#cidadeCombo').empty();

            // Adiciona a opção "Selecione"
            var option = $('<option />');
            option.attr('value', "").text("Selecione...");
            $('#cidadeCombo').append(option);

            // Adiciona as cidades do estado
            $(data).each(function () {
                var option = $('<option />');
                option.attr('value', this.cidadeId).text(this.cidade);
                // Se enviou o parâmetro cidade, seleciona a cidade
                if (cidadeSelecionada != undefined) {
                    if (cidadeSelecionada == this.cidadeId) {
                        option.attr('selected', true);
                    }
                }
                $('#cidadeCombo').append(option);
            });

            // Adiciona a opção "Adicionar Cidade"
            var option = $('<option />');
            option.attr('value', "ADD").text("Adicionar Cidade...");
            $('#cidadeCombo').append(option);
        },
        dataType: "json"
    });
}

/**
 * Oculta o bloco de adicionar nova cidade
 */
function ocultaNovaCidade() {
    $('#formSelectCidade').show();
    $('#formNovaCidade').hide();
}

/**
 * Função que preenche os campos do formulário com os dados informados
 * @param {json} data
 */
function preencheCampos(data){
    // Varre o array de dados e alimenta o formulário
    for (var prop in data) {
        $('#'+prop).val(data[prop]);
    }
    // A cidade é necessário preencher após carregar a lista a partir do estado
    carregaCidades(data['cidadeId']);
}

function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

// Código jquery
$(document).ready(function () {
    // Máscara de telefone
    $('#telefone').mask("(99) 9999-99999");

    // OnChange do Estado
    $("#estadoId").change(function () {
        // Se selecionou "Selecione..."
        if ($('#estadoId option:selected').val() == '') {
            // Limpa a combo de cidades
            $('#cidadeCombo').empty();
            // Adiciona novamente a opção "Selecione o Estado..."
            var option = $('<option />');
            option.attr('value', "ADD").text("Selecione o Estado...");
            $('#cidadeCombo').append(option);
            // Senão, carrega as cidades
        } else {
            carregaCidades();
        }
    });

    // Abre o bloco de adicionar nova cidade
    $("#cidadeCombo").change(function () {
        // Se selecionou a criação de uma nova cidade
        if ($('#cidadeCombo option:selected').val() == 'ADD') {
            $('#formSelectCidade').hide();
            $('#formNovaCidade').show();
            $('#novaCidade').focus();
        }
    });

    // Botão de cancelar inclusão de nova cidade
    $('#cancelaNovaCidade').click(function () {
        // Oculta o bloco completo
        ocultaNovaCidade();
        // Oculta a mensagem de erro, se foi exibida
        $('#formNovaCidade .msg-erro').hide();
        // Seleciona o primeiro item da combo ("Selecione...")
        $("#cidadeCombo option:first").attr('selected', 'selected');
    });

    // Botão de adicionar nova cidade
    $('#salvaNovaCidade').click(function () {
        // Salva a cidade
        $.ajax({
            type: 'POST',
            url: "contato/save-cidade",
            data: {estado: $('#estadoId option:selected').val(), cidade: $('#novaCidade').val()},
            success: function (data) {
                // Se gravou corretamente
                if (data.status == 'ok') {
                    // Oculta o formulário de inclusão de nova cidade
                    ocultaNovaCidade();
                    // Oculta a mensagem de erro, se foi exibida
                    $('#formNovaCidade .msg-erro').hide();
                    // Recarrega a combo de cidades, selecionando a nova cidade
                    carregaCidades(data.cidade.cidadeId);
                    // Limpa o campo de nova cidade
                    $('#novaCidade').val("");
                } else {
                    // Exibe a mensagem de erro
                    $('#formNovaCidade .msg-erro').html(data.message).show();
                }
            },
            dataType: "json"
        });
    });
    
    // Botão de buscar contato
    $('#buscaContato').click(function () {
        // Se não informou o nome
        if($('#nome').val() == ''){
            modalMensagem("É necessário informar um nome para fazer a busca");
        }else if($('#nome').val().length <= 2){
            modalMensagem("É necessário informar ao menos três caracteres para fazer a busca");
        }else{
            // Faz a busca a partir do nome informado
            $.ajax({
                type: 'POST',
                url: "contato/busca-contato",
                data: {nome: $('#nome').val()},
                dataType: "json",
                success: function (data) {
                    // Se não retornou dados
                    if(data.count == 0){
                        modalMensagem('Não foi encontrado nenhum registro com o nome informado.');
                    // Se retornou apenas um registro
                    }else if(data.count == 1){
                        $.ajax({
                            type: 'POST',
                            url: "contato/carrega-contato",
                            dataType: "json",
                            data: {pessoaId: data.list[0].pessoaId},
                            success: function (userData) {
                                // Preenche os campos do formulário com os dados encontrados
                                preencheCampos(userData);
                            }
                        });
                    // Se retornou vários registros
                    }else{
                        // Preenche o elemento para mostrar o valor buscado
                        $('.valor-busca').html($('#nome').val());
                        // Remove as linhas da tabela
                        $('.conteudo-busca tr:not(:first)').remove();
                        // Varre o retorno da busca
                        $(data.list).each(function () {
                            // Cria a linha
                            var tr = $('<tr />');
                            // Cria a coluna com o nome
                            var td = $('<td />');
                            //Cria o link
                            var a = $('<a />');
                            a.attr('data-pessoa-id', this.pessoaId);
                            a.attr('class', 'busca-pessoa');
                            a.css('cursor', 'pointer');
                            a.html(this.nome);
                            // Insere o link na coluna
                            $(td).append(a);
                            // Insere a coluna na linha
                            $(tr).append(td);
                            // Cria um novo link, para exibir o telefone
                            a = $('<a />');
                            a.attr('data-pessoa-id', this.pessoaId);
                            a.attr('class', 'busca-pessoa');
                            a.css('cursor', 'pointer');
                            a.html(this.telefone);
                            td = $('<td />');
                            // Insere o outro link na coluna
                            $(td).append(a);
                            // Insere a outra coluna na linha
                            $(tr).append(td);
                            // Insere a linha na tabela
                            $('.conteudo-busca').append(tr);
                        });

                        $('#modalBusca').modal('show');
                    }
                }
            });
        }
    });
    
    // Carrega as informações da pessoa a partir do link na modal
    $(document).on("click", ".busca-pessoa", function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "contato/carrega-contato",
            dataType: "json",
            data: {pessoaId: $(this).attr('data-pessoa-id')},
            success: function (userData) {
                // Preenche os campos do formulário com os dados encontrados
                preencheCampos(userData);
                $('#modalBusca').modal('hide');
            }
        });
    });
    
    // Regras customizadas do validate
    // Select preenchido
    $.validator.addMethod("selectRequired", function(value){
        return (value != '' && value != 'ADD' && value != null);
    }, "Selecione uma opção");
    
    // É telefone
    function eTelefone(numero) {
        ///^\(\d{2}\) \d{4}\-\d{4}?$/ 
        numero = numero.replace('_', "");
        return /^\(\d{2}\) [2-9]{1}\d{3}\-(\d{4}|\d{5})$/.test(numero);
    }
    $.validator.addMethod("telefone", function(value, element, type) {
        return this.optional(element) || eTelefone(value);
    }, "Digite um telefone válido");

    // Regras do validate no formulario
    $('#formContato').validate({
        rules: {
            pessoaTipoId: {selectRequired: true},
            nome: {required: true},
            telefone: {required: true, telefone: true},
            origemId: {selectRequired: true},
            cidadeId: {selectRequired: true}
        },
        messages: {
            pessoaTipoId: {selectRequired: 'É necessário selecionar um tipo de pessoa'},
            nome: {required: 'É necessário informar o nome'},
            telefone : {required: 'É necessário informar um telefone', telefone : 'Telefone inválido'},
            origemId: {selectRequired: 'É necessário selecionar uma origem do contato'},
            cidadeId: {selectRequired: 'É necessário selecionar uma cidade'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            var dados = $("#formContato").serialize();

            $.ajax({
                type: "POST",
                url: "contato/save-contato",
                data: dados,
                dataType: 'json',
                success: function (data){
                    if(data.status == 'ok'){
                        $('#pessoaId').val(data.pessoaId);
                        modalMensagem('Contato salvo com sucesso');
                    }else{
                        modalMensagem(data.message);
                    }
                }
            });
            return false;
        }
    });
});