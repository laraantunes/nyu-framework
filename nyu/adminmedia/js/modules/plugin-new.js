$(function(){
    // Regras do validate no formulario
    $('#plugin_new_form').validate({
        rules: {
            plugin: {required: true}
        },
        messages: {
            plugin: {required: 'É necessário informar o Id do plugin'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            var dados = $("#plugin_new_form").serialize();
            $.ajax({
                type: "POST",
                url: $('#plugin_new_form').attr('action'),
                data: dados,
                dataType: 'json',
                success: function (data){
                    if(data.status){
                        document.location.href = NYU_ADMIN_URL + 'plugin/edit/' + data.plugin;
                    }else{
                        modalMensagem(data.msg);
                    }
                }
            });
            return false;
        }
    });
});