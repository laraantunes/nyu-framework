function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}
$(function(){
    $(".menu-delete").click(function(){
        title = $(this).attr('data-title');
        if(confirm("Confirma a exclusão do menu "+title+"? Todos os menus filhos serão excluídos.")){
            id = $(this).attr('data-id');
            $.ajax({
                    type: "POST",
                    url: NYU_ADMIN_URL+'menu/delete/',
                    data: {'menu_tree_item' : id},
                    dataType: 'json',
                    success: function (data){
                        if(data.status){
                                window.location.href = window.location.href;
                        }else{
                                modalMensagem(data.msg);
                        }
                    }
            });
        }
    });
});