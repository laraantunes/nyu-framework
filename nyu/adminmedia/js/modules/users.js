function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

$(function(){
	$(".user-delete").click(function(){
		username = $(this).attr('data-username');
        if(confirm("Confirma a exclusão do usuário "+username+"?")){
        	userid = $(this).attr('data-id');
            $.ajax({
	            type: "POST",
	            url: NYU_ADMIN_URL+'user/delete/',
	            data: {'user' : userid},
	            dataType: 'json',
	            success: function (data){
	            	if(data.status){
	            		window.location.href = window.location.href;
	            	}else{
	                	modalMensagem(data.msg);
                	}
	            }
	        });
        }
    });
});
