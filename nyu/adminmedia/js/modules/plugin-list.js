$(function () {
    $(".plugin-delete").click(function () {
        pluginname = $(this).attr('data-pluginname');
        if (confirm("Confirma a exclusão do plugin " + pluginname + "?")) {
            plugin = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: NYU_ADMIN_URL + 'plugin/delete/',
                data: {'plugin': plugin},
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        window.location.href = window.location.href;
                    } else {
                        modalMensagem(data.msg);
                    }
                }
            });
        }
    });
});
