var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/json");

$(function(){
    $("#save-content").click(function(){
        var code = editor.getValue();
        var plugin = $('#plugin').val();
        $.ajax({
            type: "POST",
            url: NYU_ADMIN_URL+'plugin/save-config/',
            data: {'content' : code, 'plugin' : plugin},
            dataType: 'json',
            success: function (data){
                modalMensagem(data.msg);
            }
        });
    });
    
    $("#refresh").click(function(){
        window.location.href = window.location.href;
    });
});