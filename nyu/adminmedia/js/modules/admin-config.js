function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

$(function(){
    // Regras do validate no formulario
    $('#nyu_admin_login').validate({
        rules: {
            username: {required: true},
            password: {required: true}
        },
        messages: {
            username: {required: 'É necessário informar o nome do usuário'},
            password: {required: 'É necessário informar a senha'}
        },
        onkeyup: false,
        submitHandler: function (form) {
            var dados = $("#nyu_admin_login").serialize();
            $.ajax({
                type: "POST",
                url: $("#nyu_admin_login").attr('action'),
                data: dados,
                dataType: 'json',
                success: function (data){
                    if(data.status){
                        window.location.href = NYU_ADMIN_URL;
                    }else{
                        modalMensagem(data.msg);
                    }
                }
            });
            return false;
        }
    });
    
    $('#username').keydown(function (e) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 13 || e.keyCode == 9 || 
            e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || 
            e.keyCode == 20 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 45 || 
            e.shiftKey || e.altKey || e.ctrlKey){
            return true;
        }
        var regex = new RegExp("^[a-zA-Z0-9\.\_]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    
    $('#nickname').keydown(function (e) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 13 || e.keyCode == 9 || 
            e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || 
            e.keyCode == 20 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 45 || 
            e.shiftKey || e.altKey || e.ctrlKey){
            return true;
        }
        var accentedCharacters = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ";
        var regex = new RegExp("^[a-zA-Z0-9"+accentedCharacters+"\.\_]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    
    $("#reinstallbtn").click(function(){
        if(confirm("Confirma a reinstalação da administração do sistema?")){
            window.location.href = NYU_ADMIN_URL+'install/redo/';
        }
    });
});