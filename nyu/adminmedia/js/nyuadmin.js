function modalMensagem(msg){
    $('.modalMensagemConteudo').html(msg);
    $('#modalMensagem').modal('show');
}

function logoff(){
    $.ajax({
        type: "GET",
        url: NYU_ADMIN_URL+"user/logoff",
        dataType: 'json',
        success: function (data){
            if(data.status){
                window.location.href = NYU_ADMIN_URL;
            }
        }
    });
}

$(function(){
    jQuery(document).on("click", ".nyuadmin-logoff", function(e){
        e.preventDefault();
        logoff();
    });
});

